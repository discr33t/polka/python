# python

Install and manage Python

## Dependenices

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of python versions to install

* `global_version`:
    * Type: String
    * Usages: Python version to make global default

```
python:
  versions:
    - 3.6.6
    - 3.7.5
  global_version: 3.7.5
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.python

## License

MIT
